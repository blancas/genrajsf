package it.strazz.primefaces.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PrivilegioDTO {
    public static final String PRIV_AUTO="PRIV_AUTO";
    public static final String PRIV_EDICION="PRIV_EDICION";
    public static final String PRIV_SEGURIDAD="PRIV_SEGURIDAD";
    private String clave;
    private String descripcion; 
}
