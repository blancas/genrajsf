package it.strazz.primefaces.model;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecursoSeguroDTO implements Serializable 
{
	private static final long serialVersionUID = -7029223536242660708L;
	private String recurso;
    private String descripcion;
    private RecursoSeguroDTO recursoPadre;
    @Override
    public String toString() {
        return "RecursoSeguroDTO{" +
                "recurso='" + recurso + '\'' +
                '}';
    } 
}
