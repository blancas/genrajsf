package it.strazz.primefaces.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class UsuarioDTO implements Serializable 
{
	private static final long serialVersionUID = 7161463618055141822L;
	private String username;
    private String password;
    private Integer enabled;
    private Integer deleted;
    private String nombre;
    private String email;
    private Integer locked;
    private Instant ultimoCambioPassword;
    private Instant ultimoAcceso;
    private Short  numIntentosLogin;
    private Instant bloqueoIntentos;
    private Integer enrolado;
    private String ip;
    private Instant fechaRegistro;
    private String sessionId;
    private Integer cambiarPassword;
    private String usuarioRegistro;
    private List<RolDTO> roles;
    private List<PrivilegioDTO> privilegios;
    public UsuarioDTO(
    		String username,
		    String password, 
		    String nombre,
		    String email,
		    Integer locked,
		    Short  numIntentosLogin,  
		    String ip,
		    String sessionId,
		    Integer cambiarPassword )
    {
    	this.username =username;
    	this.password =  password;
    	this.nombre=  nombre;
    	this.email =  email;
    	this.locked =  locked;
    	this.numIntentosLogin =  numIntentosLogin;
    	this.ip =  ip;
    	this.sessionId =  sessionId;
    	this.cambiarPassword =  cambiarPassword;
    	
    }    
}
