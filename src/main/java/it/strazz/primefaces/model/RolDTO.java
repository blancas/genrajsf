package it.strazz.primefaces.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RolDTO implements Serializable 
{
	private static final long serialVersionUID = -1917484203171527290L;
	public static final String TIPO_NUEVO="N";
    public static final String TIPO_MODIFICACION="M";
    public static final String TIPO_ELIMINACION="E";
    private String clave;
    private String nombre;
    private List<RecursoSeguroDTO> permisos;
    private UsuarioDTO usuario;
    private Instant tsModificacion;
    private String tipo;
    private String actividad;
    private boolean disable;
    public RolDTO(String clv, String nmbr,String act,boolean st)
    {
    	this.clave =  clv;
    	this.nombre = nmbr;
    	this.actividad =act;
    	this.disable = st; 
    }
	@Override
	public String toString() {
		return "{'clave':'" + clave + "','nombre':'" + nombre +  "'}";
	} 
    

    
}
