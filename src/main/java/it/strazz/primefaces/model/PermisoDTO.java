package it.strazz.primefaces.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermisoDTO {
    public static final String TIPO_NUEVO="N";
    public static final String TIPO_MODIFICACION="M";
    public static final String TIPO_ELIMINACION="E";
    private String clave;
    private String nombre;
    private List<RecursoSeguroDTO> permisos;
    private UsuarioDTO usuario;
    private Instant tsModificacion;
    private String tipo;
    public PermisoDTO(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }
}
