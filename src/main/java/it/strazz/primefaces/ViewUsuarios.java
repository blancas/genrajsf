package it.strazz.primefaces; 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import it.strazz.primefaces.model.UsuarioDTO;
import lombok.Getter;
import lombok.Setter;
@ManagedBean
@ViewScoped
@Getter
@Setter
public class ViewUsuarios implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4557001946729868576L;
	private List<UsuarioDTO> columns = new ArrayList<UsuarioDTO>(0);
	private String username;
	private String password; 
	private String nombre;
	private String email;
	private Integer locked;
	private Short  numIntentosLogin;  
	private String ip;
	private String sessionId;
	private Integer cambiarPassword;
	@PostConstruct
	public void init()
	{
		columns.add(new UsuarioDTO(
	    		"AD0001",
			    "AD0001L", 
			    "Luis","lbln01@gmail.com",1,(short)0,"0.0.0.0","Session 001",30));
		columns.add(new UsuarioDTO(
	    		"AD0002",
			    "AD0002L", 
			    "Adrian","lbln01@gmail.com",1,(short)0,"0.0.0.0","Session 001",30));
		columns.add(new UsuarioDTO(
	    		"AD0003",
			    "AD0003L", 
			    "Blancas","lbln01@gmail.com",1,(short)0,"0.0.0.0","Session 001",30));
	}
	public UsuarioDTO insertOne(String username,
		    String password, 
		    String nombre,
		    String email,
		    Integer locked,
		    Short  numIntentosLogin,  
		    String ip,
		    String sessionId,
		    Integer cambiarPassword)
	{
		UsuarioDTO newUser= new UsuarioDTO(
				 username,
				    password, 
				    nombre,
				    email,
				    locked,
				    numIntentosLogin,  
				    ip,
				    sessionId,
				    cambiarPassword);
		columns.add(newUser);
		return findOne(username);
	}
	public List<UsuarioDTO > findAll() 
	{
		return columns;
	}
	public UsuarioDTO findOne(String clave)
	{
		UsuarioDTO find=null;
		for(UsuarioDTO dto : columns)
		{
			if(dto.getUsername().toUpperCase().equals(clave.toUpperCase()))
				return dto;
		}
		return find;
	}
	public UsuarioDTO updateOne(String clave,UsuarioDTO dtoNew)
	{
		UsuarioDTO find=null;
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			UsuarioDTO dto =  columns.get(cont);
			if(dto.getUsername().toUpperCase().equals(clave.toUpperCase()))
			{
				columns.get(cont).setUsername(dtoNew.getUsername());
				columns.get(cont).setPassword(dtoNew.getPassword());
				columns.get(cont).setNombre(dtoNew.getNombre());
				columns.get(cont).setEmail(dtoNew.getEmail());
				columns.get(cont).setLocked(dtoNew.getLocked());
				columns.get(cont).setNumIntentosLogin(dtoNew.getNumIntentosLogin());
				columns.get(cont).setIp(dtoNew.getIp());
				columns.get(cont).setSessionId(dtoNew.getSessionId());
				columns.get(cont).setCambiarPassword(dtoNew.getCambiarPassword());
				return columns.get(cont);
			}
		}
		return find;
	}
	public List<UsuarioDTO> deleteOne(String clave,UsuarioDTO dtoNew)
	{
		List<UsuarioDTO> columnsNew = new ArrayList<UsuarioDTO>(0);
		UsuarioDTO find=null;
		for(UsuarioDTO dto : columns)
		{
			if(dto.getUsername().toUpperCase().equals(clave.toUpperCase()))
				System.out.println("STO  Borrado "+dto.getUsername());
			else
				columnsNew.add(dto);
		}
		columns= columnsNew;
		return findAll();
	}
}
