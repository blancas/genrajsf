package it.strazz.primefaces;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import it.strazz.primefaces.model.PrivilegioDTO;
import lombok.Getter;
import lombok.Setter;
@ManagedBean
@ViewScoped
@Getter
@Setter
public class ViewPrivilegio implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2508387177346303129L;
	private List<PrivilegioDTO> columns = new ArrayList<PrivilegioDTO>(0);
	private String clave;
    private String descripcion;
	@PostConstruct
	public void init()
	{
		columns.add(new PrivilegioDTO("AD0001","Seguridad"));
		columns.add(new PrivilegioDTO("AD0002","Consulta"));
		columns.add(new PrivilegioDTO("AD0003","Anexar"));
	}
	public PrivilegioDTO insertOne(String clave,String descripcion)
	{
		PrivilegioDTO newUser= new PrivilegioDTO(clave,descripcion);
		columns.add(newUser);
		return findOne(clave);
	}
	public List<PrivilegioDTO > findAll() 
	{
		return columns;
	}
	public PrivilegioDTO findOne(String clave)
	{
		PrivilegioDTO find=null;
		for(PrivilegioDTO dto : columns)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
				return dto;
		}
		return find;
	}
	public PrivilegioDTO updateOne(String clave,PrivilegioDTO dtoNew)
	{
		PrivilegioDTO find=null;
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			PrivilegioDTO dto =  columns.get(cont);
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
			{
				columns.get(cont).setClave(dtoNew.getClave());
				columns.get(cont).setDescripcion(dtoNew.getDescripcion());
				return columns.get(cont);
			}
		}
		return find;
	}
	public List<PrivilegioDTO> deleteOne(String clave,PrivilegioDTO dtoNew)
	{
		List<PrivilegioDTO> columnsNew = new ArrayList<PrivilegioDTO>(0);
		PrivilegioDTO find=null;
		for(PrivilegioDTO dto : columns)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
				System.out.println("STO  Borrado "+dto.getClave());
			else
				columnsNew.add(dto);
		}
		columns= columnsNew;
		return findAll();
	}
}

