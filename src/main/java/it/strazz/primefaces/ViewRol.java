package it.strazz.primefaces;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DualListModel;

import it.strazz.primefaces.model.RolDTO;
import lombok.Getter;
import lombok.Setter;
@ManagedBean
@ViewScoped
@Getter
@Setter
public class ViewRol implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2795093864977779325L;
	private final FacesContext context = FacesContext.getCurrentInstance();
	private List<RolDTO> columns = new ArrayList<RolDTO>(0);
	private List<RolDTO> columnsXA = new ArrayList<RolDTO>(0);
	List<String> columnsXAporBorrarEmpty   = new ArrayList<String>(0);
	List<String> columnsXAporInsertarEmpty = new ArrayList<String>(0);
	private DualListModel<String> porBorrar;
	private DualListModel<String> porInsertar;
	private int countInsertar;
	private int countBorrar;
	private RolDTO beanSelected;
	private RolDTO beanRespaldado;
	private RolDTO beanNuevo=new RolDTO();
	private String clave;
    private String nombre;
	@PostConstruct
	public void init()
	{
		countInsertar=0;
		countBorrar=0;
		columns.add(new RolDTO(
	    		"CLAVE001","ROL_INICIAL","",false));
		columns.add(new RolDTO(
				"CLAVE002","ROL_ADMIN","",false));
	} 
	public String insertOneStr(String clave)
	{
		System.out.println("Se inserto !!" +clave);
		RolDTO dto = insertOne(clave);
		imprimir();
		this.nombre ="";
		return "";
	}
	public RolDTO insertOne(String nm)
	{
		String clv= ""+(new Date()).getTime();
		RolDTO newUser= new RolDTO(clv,nm,"Por insertar",true);
		columnsXA.add(newUser);
		return findOne(clv);
	}
	public List<RolDTO > findAll() 
	{
		return columns;
	}
	public RolDTO findOne(String clave)
	{
		RolDTO find=null;
		for(RolDTO dto : columns)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
				return dto;
		}
		return find;
	}
	public void seleccionar(RolDTO dto)
	{
		System.out.println("Se Selecciono "+dto.toString());
		RolDTO beanSelected = new RolDTO();
		beanSelected.setClave(dto.getClave());
		beanSelected.setNombre(dto.getNombre());
		beanRespaldado= new RolDTO();
		beanRespaldado.setClave(dto.getClave());
		beanRespaldado.setNombre(dto.getNombre());
	}
	public String updateOneStr(String clave,RolDTO dtoUp)
	{
		RolDTO dto = updateOne(clave,dtoUp);
		imprimir();
		return "";
	}
	public RolDTO updateOne(String clave,RolDTO Upd)
	{
		System.out.println("Se Edito ["+clave+"] " + Upd.toString());
		System.out.println("Se Back  ["+clave+"] " + beanRespaldado.toString());
		RolDTO find=null;
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			RolDTO dto =  columns.get(cont);
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
			{
				columnsXA.add(Upd);
				columns.get(cont).setNombre(beanRespaldado.getNombre());
				columns.get(cont).setActividad("Por modificar");
				columns.get(cont).setDisable(true);
				return columns.get(cont);
			}
		}
		return find;
	}
	public String deleteOneStr(RolDTO dto)
	{
		System.out.println("Se Borro "+dto.toString());
		columns = deleteOne(dto);
		imprimir();
		return "";
	}
	public List<RolDTO> deleteOne(RolDTO dtoDel)
	{
		System.out.println("A  Borrar "+dtoDel.toString());
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			if(columns.get(cont).getClave().toUpperCase().equals(dtoDel.getClave().toUpperCase()))
			{
				columnsXA.add(columns.get(cont));
				columns.get(cont).setActividad("Por borrar");
				columns.get(cont).setDisable(true);
			}
		}
		return findAll();
	}
	public void imprimir()
	{
		System.out.println("***************************************************************");
		System.out.println("*************************XA************************************");
		for(RolDTO dto : columnsXA )
			System.out.println("****::"+dto.toString());
		System.out.println("=========================NORMAL=================================");
		for(RolDTO dto : columns )
			System.out.println("****::"+dto.toString());
		System.out.println("***************************************************************");
		System.out.println("***************************************************************");
	}
	public String autoritation(DualListModel<RolDTO> pend)
	{
		System.out.println(" this.pendientes.Source.size :  "+pend.getSource().size() );
		System.out.println(" this.pendientes.Target.size :  "+pend.getTarget().size() );
		return  "";
	}
	public int getCountBorrar()
	{
		System.out.println(" columnsXA.size :  "+columnsXA.size());
		List<RolDTO> columnsporBorrar = new ArrayList<RolDTO>(0);
		for(RolDTO esPorBorrar :columnsXA )
		{
			if(esPorBorrar.getActividad().equals("Por borrar"))
				columnsporBorrar.add(esPorBorrar);
		}
		return  columnsporBorrar.size();
	}
	public int getCountInsertar()
	{
		System.out.println(" columnsXA.size :  "+columnsXA.size());
		List<RolDTO> columnsporInsertar = new ArrayList<RolDTO>(0);
		for(RolDTO esPorInsertar :columnsXA )
		{
			if(esPorInsertar.getActividad().equals("Por insertar"))
				columnsporInsertar.add(esPorInsertar);
		}
		return  columnsporInsertar.size();
	}
	public DualListModel<String> getPorBorrar()
	{
		System.out.println(" columnsXA.size :  "+columnsXA.size());
		List<String> columnsporBorrar = new ArrayList<String>(0);
		for(RolDTO esPorBorrar :columnsXA )
		{
			if(esPorBorrar.getActividad().equals("Por borrar"))
				columnsporBorrar.add(esPorBorrar.toString());
		}
		this.porBorrar =  new   DualListModel<String>(columnsporBorrar, columnsXAporBorrarEmpty);
		System.out.println(" this.pendientes.Source.size :  "+this.porBorrar.getSource().size() );
		System.out.println(" this.pendientes.Target.size :  "+this.porBorrar.getTarget().size() );
		return  this.porBorrar ;
	}
	public DualListModel<String> getPorInsertar()
	{
		System.out.println(" columnsXA.size :  "+columnsXA.size());
		List<String> columnsporInsertar = new ArrayList<String>(0);
		for(RolDTO esPorInsertar :columnsXA )
		{
			if(esPorInsertar.getActividad().equals("Por insertar"))
				columnsporInsertar.add(esPorInsertar.toString());
		}
		this.porInsertar =  new   DualListModel<String>(columnsporInsertar, columnsXAporInsertarEmpty);
		System.out.println(" this.pendientes.Source.size :  "+this.porInsertar.getSource().size() );
		System.out.println(" this.pendientes.Target.size :  "+this.porInsertar.getTarget().size() );
		return  this.porInsertar;
	}
	public String autoritationPorBorrar()
	{
		Gson gson = new Gson();
		System.out.println("*************************autoritationPorBorrar******************************");
		System.out.println(" this.PorBorrar.Source.size :  "+this.porBorrar.getSource().size() );
		System.out.println(" this.PorBorrar.Target.size :  "+this.porBorrar.getTarget().size() );
		System.out.println("*************************EXCLUIDOS******************************");
		for(String excluido :this.porBorrar.getSource())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("excluido :: "+dto.getClave());
		}
		System.out.println("*************************BORRADOS******************************");
		for(String excluido :this.porBorrar.getTarget())
		{
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("borrado :: "+dto.getClave());
			this.columns= forListDeleteOne( this.columns , dto);
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);
		}
		return "";
	}
	public String inAutoritationPorBorrar()
	{
		Gson gson = new Gson();
		System.out.println("*************************autoritationPorBorrar******************************");
		System.out.println(" this.PorBorrar.Source.size :  "+this.porBorrar.getSource().size() );
		System.out.println(" this.PorBorrar.Target.size :  "+this.porBorrar.getTarget().size() );
		System.out.println("*************************EXCLUIDOS******************************");
		for(String excluido :this.porBorrar.getSource())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("Eliminar de XA :: "+dto.getClave());
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);
			modifyListUpdate(dto);
		}
		for(String excluido :this.porBorrar.getTarget())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("Eliminar de XA :: "+dto.getClave());
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);
			modifyListUpdate(dto);
		}
		return "";
	}
	private void modifyListUpdate(RolDTO dto) 
	{
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			if(columns.get(cont).getClave().toUpperCase().equals(dto.getClave().toUpperCase()))
			{
				columns.get(cont).setActividad("");
				columns.get(cont).setDisable(false);
			}
		}
	}
	public List<RolDTO> forListDeleteOne(List<RolDTO> lista,RolDTO dtoDel)
	{
		List<RolDTO> newList =new ArrayList<RolDTO>();
		for(RolDTO del : lista)
		{
			if(del.getClave().toUpperCase().equals(dtoDel.getClave().toUpperCase()))
				System.out.println("excluido :: "+del.getClave());
			else
				newList.add(del);
		}
		return newList;
	}
	public String autoritationPorInsertar()
	
	{
		Gson gson = new Gson();
		System.out.println("*************************autoritationPorInsertar******************************");
		System.out.println(" this.PorInsertar.Source.size :  "+this.porInsertar.getSource().size() );
		System.out.println(" this.PorInsertar.Target.size :  "+this.porInsertar.getTarget().size() );
		System.out.println("*************************EXCLUIDOS******************************");
		for(String excluido :this.porInsertar.getSource())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("excluido :: "+dto.getClave());
		}
		System.out.println("*************************BORRADOS******************************");
		for(String excluido :this.porInsertar.getTarget())
		{
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("borrado :: "+dto.getClave());
			RolDTO insert  = findOneForList(this.columnsXA,dto.getClave());
			this.columns.add(insert);
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);
		}
		return "";
	}
	public String inAutoritationPorInsertar()
	
	{
		Gson gson = new Gson();
		System.out.println("*************************autoritationPorInsertar******************************");
		System.out.println(" this.PorInsertar.Source.size :  "+this.porInsertar.getSource().size() );
		System.out.println(" this.PorInsertar.Target.size :  "+this.porInsertar.getTarget().size() );
		System.out.println("*************************EXCLUIDOS******************************");
		for(String excluido :this.porInsertar.getSource())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("Eliminar de XA :: "+dto.getClave());
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);			
		}
		for(String excluido :this.porInsertar.getTarget())
		{ 
			RolDTO dto = gson.fromJson(excluido,RolDTO.class);
			System.out.println("Eliminar de XA :: "+dto.getClave());
			this.columnsXA= forListDeleteOne( this.columnsXA , dto);			
		}
		return "";
	}
	public RolDTO findOneForList(List<RolDTO> lista,String clave)
	{
		RolDTO find=null;
		for(RolDTO dto : lista)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
			{
				dto.setActividad("");
				dto.setDisable(false);
				return dto;
			}
		}
		return find;
	}
}
