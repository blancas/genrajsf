package it.strazz.primefaces;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import it.strazz.primefaces.model.RecursoSeguroDTO;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class ViewRecursoSeguro implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3222120454604100724L;
	private List<RecursoSeguroDTO> columns = new ArrayList<RecursoSeguroDTO>(0);
	private String recurso;
    private String descripcion;
    private RecursoSeguroDTO recursoPadre;
	@PostConstruct
	public void init()
	{
		columns.add(new RecursoSeguroDTO(
	    		"/archivos/recurso.xhmlt",
			    "Archivos de recursos",null));
		columns.add(new RecursoSeguroDTO(
				"/archivos/login.xhmlt",
			    "Archivos de Login",null));
		columns.add(new RecursoSeguroDTO(
				"/archivos/pagina.xhmlt",
			    "Archivos de Pagina",null));
	}
	public RecursoSeguroDTO insertOne(String recurso,String descripcion,RecursoSeguroDTO recursoPadre)
	{
		RecursoSeguroDTO newDto= new RecursoSeguroDTO(
				 recurso,descripcion,recursoPadre);
		columns.add(newDto);
		return findOne(recurso);
	}
	public List<RecursoSeguroDTO > findAll() 
	{
		return columns;
	}
	public RecursoSeguroDTO findOne(String clave)
	{
		RecursoSeguroDTO find=null;
		for(RecursoSeguroDTO dto : columns)
		{
			if(dto.getRecurso().toUpperCase().equals(clave.toUpperCase()))
				return dto;
		}
		return find;
	}
	public RecursoSeguroDTO updateOne(String clave,RecursoSeguroDTO dtoNew)
	{
		RecursoSeguroDTO find=null;
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			RecursoSeguroDTO dto =  columns.get(cont);
			if(dto.getRecurso().toUpperCase().equals(clave.toUpperCase()))
			{
				columns.get(cont).setRecurso(dtoNew.getRecurso());
				columns.get(cont).setDescripcion(dtoNew.getDescripcion());
				columns.get(cont).setRecursoPadre(dtoNew.getRecursoPadre());
				return columns.get(cont);
			}
		}
		return find;
	}
	public List<RecursoSeguroDTO> deleteOne(String clave,RecursoSeguroDTO dtoNew)
	{
		List<RecursoSeguroDTO> columnsNew = new ArrayList<RecursoSeguroDTO>(0);
		for(RecursoSeguroDTO dto : columns)
		{
			if(dto.getRecurso().toUpperCase().equals(clave.toUpperCase()))
				System.out.println("DTO  Borrado "+dto.getRecurso());
			else
				columnsNew.add(dto);
		}
		columns= columnsNew;
		return findAll();
	}
}

