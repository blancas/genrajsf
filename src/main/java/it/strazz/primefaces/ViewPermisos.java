package it.strazz.primefaces;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import it.strazz.primefaces.model.PermisoDTO;
import lombok.Getter;
import lombok.Setter;
@ManagedBean
@ViewScoped
@Getter
@Setter
public class ViewPermisos implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7095885296536606114L;
	private List<PermisoDTO> columns = new ArrayList<PermisoDTO>(0);
	private String clave;
    private String nombre;
	@PostConstruct
	public void init()
	{
		columns.add(new PermisoDTO("AD0001","AD0001L"));
		columns.add(new PermisoDTO("AD0001","AD0001L"));
		columns.add(new PermisoDTO("AD0001","AD0001L"));
	}
	public PermisoDTO insertOne(String clave,String nombre)
	{
		PermisoDTO newUser= new PermisoDTO(clave,nombre);
		columns.add(newUser);
		return findOne(clave);
	}
	public List<PermisoDTO > findAll() 
	{
		return columns;
	}
	public PermisoDTO findOne(String clave)
	{
		PermisoDTO find=null;
		for(PermisoDTO dto : columns)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
				return dto;
		}
		return find;
	}
	public PermisoDTO updateOne(String clave,PermisoDTO dtoNew)
	{
		PermisoDTO find=null;
		for(int cont = 0;cont < columns.size() ;cont++)
		{
			PermisoDTO dto =  columns.get(cont);
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
			{
				columns.get(cont).setClave(dtoNew.getClave());
				columns.get(cont).setNombre(dtoNew.getNombre());
				return columns.get(cont);
			}
		}
		return find;
	}
	public List<PermisoDTO> deleteOne(String clave,PermisoDTO dtoNew)
	{
		List<PermisoDTO> columnsNew = new ArrayList<PermisoDTO>(0);
		PermisoDTO find=null;
		for(PermisoDTO dto : columns)
		{
			if(dto.getClave().toUpperCase().equals(clave.toUpperCase()))
				System.out.println("STO  Borrado "+dto.getClave());
			else
				columnsNew.add(dto);
		}
		columns= columnsNew;
		return findAll();
	}
}

